-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

--[[
	這個範例解釋如何宣告並呼叫function，以及彈性上的應用
	Author: Zack Lin
	Time: 2015/3/12
]]

--本地function宣告
local function doIt(what , where)
	print( what .. ",在" .. where )  
end
--function呼叫方式
doIt("作運動","電影院")

--function可作為變量來進行賦值
local method = doIt
method("玩電腦","游泳池")

--宣告無定量參數的function，並展示如何取出每一個參數
local function lotsOfArgs(...)
	for i, v in ipairs(arg) do
		print("第" .. i .."個參數為" .. v)
	end

	--判斷參數的型別，可能回傳值為string.number.table
	print("第一個參數的型別為" .. type(arg[1]))
end

lotsOfArgs("倚天劍","屠龍刀","誰與爭鋒","Hello World")

--將某個Table的元素宣告為function
local t = {}
t.doIt = function ( what , where )
	print( what ..",在" .. where )
end
t.doIt("睡覺","廁所")


--宣告可回傳值的function
local function  add( a , b )
	return a + b
end

local result = add (1 , 2)
print( result )